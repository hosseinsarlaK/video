<?php

namespace App\Http\Controllers;
use App\Models\Video;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Filters\VideoFilters;




class CategoryVideoController extends Controller
{
    
    public function index(Request $request,Category $category)
    {  
        $videos = $category
            ->videos()
            ->paginate()
            ->withQuerystring();
            // ->filter($request->all());    
        $title = $category->name;
        return view('videos.index', compact('videos', 'title'));
    }
}
