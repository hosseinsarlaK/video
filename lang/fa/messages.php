<?php

return [
    'success'=> 'عملیات شماموفقیت امیزبود',
    'video_edited' => '.ویدیو شما باموفقیت بروزرسانی شد',
    'please_verify_your_email' => '.برای بهرمند شدن از تمامی امکانات سایت لطفا ایمیل خود را تایید کنید',
    'verification_email_was_sent' => '.ایمیل تاییدیه ارسال شد',
    'your_email_was_verified' => '.ایمیل شماباموفقعیت تایید شد',
    'reset_link_was_sent' => '.ایمیل بازیابی رمز عبور ارسال شد',
    'your_password_was_reset' => '.رمزعبور با موفقعیت بازیابی شد',
    'comment_sent_successfully' => 'نظر شماباموفقعیت ثبت شد '


];
